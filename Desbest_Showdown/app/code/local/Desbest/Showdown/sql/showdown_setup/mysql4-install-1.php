<?php
//echo 'Running This Upgrade: '.get_class($this)."\n <br /> \n"; die("Exit for now");

$installer = $this;
//@var $installer->Mage_Catalog_Model_Resource_Eav_Mysql4_Setup
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('showdown_matches')};
CREATE TABLE {$this->getTable('showdown_matches')} (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `firstproductid` int(11) NOT NULL,
      `secondproductid` int(11) NOT NULL,
      `title` varchar(255) NOT NULL,
      `datenumber` varchar(10) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
    
    CREATE TABLE IF NOT EXISTS `showdown_votes` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `matchid` int(11) NOT NULL,
      `votedfor` varchar(10) NOT NULL,
      `ip` varchar(50) NOT NULL,
      `datenumber` varchar(10) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



");
$installer->endSetup();