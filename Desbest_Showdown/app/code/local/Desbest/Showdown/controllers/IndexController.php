<?php
class Desbest_Showdown_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        
        $this->loadLayout(); 
        
        //getView() function
        $params = $this->getRequest()->getParams();
        if (!empty($params)){  $viewthis = $params['c']; }
          else { $viewthis = 1; }
        
        $storeId = Mage::app()->getStore()->getId();
        $mymatch = Mage::getModel('showdown/matches')->load($viewthis);
        $product1 = Mage::helper('catalog/product')->getProduct($mymatch->getFirstproductid(), $storeId);
        $product2 = Mage::helper('catalog/product')->getProduct($mymatch->getSecondproductid(), $storeId);

        $head = $this->getLayout()->getBlock('head');
        $head->setTitle($mymatch->getTitle());
        $head->setDescription("".$product1->getTitle()." and ".$product2->getTitle()." go head to head to decide which is your favourite product.");
        $head->setKeywords("".$product1->getMetaKeyword()." ".$product2->getMetaKeyword() );
        
        $this->renderLayout();
    }
    
    public function travelAction(){
        $params = $this->getRequest()->getParams();
        $datenumber = $params['date'];
        
        $rake = Mage::getModel('showdown/matches')->getCollection();
        $rake->addFieldToFilter('datenumber', $datenumber);
        //$rake->setPage(1,2);
        $rake->load();
        $count = count($rake);
        
        if ($count == 0) { 
            $this->_redirect('showdown/index', array('c' => 1) ); 
        } else { 
            for($i=1;$i==1;$i++) { foreach($rake as $entry){
                $this->_redirect('showdown/index', array('c' => $entry['id']) );
            } }            
        }
        
    }
    
    public function voteAction(){
        $params = $this->getRequest()->getParams(); 
        $r = $params['r']; $productid = $params['id'];
        $ip=$_SERVER['REMOTE_ADDR'];
        $datenumber = date("Y-m-d");
        
            $rakevote = Mage::getModel('showdown/votes')->getCollection();
            $rakevote->addFieldToFilter('matchid', $r);
            $rakevote->addFieldToFilter('ip', $ip);
            $sticks = count($rakevote);
        if ($sticks == 0){
            $makevote = Mage::getModel('showdown/votes');
            $makevote->setIp($ip);
            $makevote->setMatchid($r);
            $makevote->setVotedfor($productid);
            $makevote->setDatenumber($datenumber);
            $makevote->save();
        }
        $matchcount = Mage::getResourceModel('showdown/matches')->count;
        $random = rand(1,$matchcount);
        $this->_redirect('showdown/index', array('c' => $matchcount) );
        //$this->_redirect('showdown/index', array('c' => $r) );
    }
    
    public function mockcron_newmatchAction(){
        $task = Mage::getModel('showdown/cron')->makematch();
        var_dump($task);
    }
    
}