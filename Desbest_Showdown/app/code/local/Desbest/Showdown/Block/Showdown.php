<?php
class Desbest_Showdown_Block_Showdown extends Mage_Core_Block_Template
{
    public function getView(){
        $params = $this->getRequest()->getParams();
        if (!empty($params)){  $viewthis = $params['c']; }
          else { $viewthis = 1; }
        return $viewthis;
    }
    
    public function haveChosen($productid)
    {
        $ip=$_SERVER['REMOTE_ADDR'];
        $rakevote = Mage::getModel('showdown/votes')->getCollection();
        $rakevote->addFieldToFilter('votedfor', $productid);
        $rakevote->addFieldToFilter('ip', $ip);
        $sticks = count($rakevote);
        if ($sticks > 0){ return true; } else { return false; }
    }
    
    public function haveVoted($matchid)
    {
        $ip=$_SERVER['REMOTE_ADDR'];
        $rakevote = Mage::getModel('showdown/votes')->getCollection();
        $rakevote->addFieldToFilter('matchid', $matchid);
        $rakevote->addFieldToFilter('ip', $ip);
        $sticks = count($rakevote);
        if ($sticks > 0){ return true; } else { return false; }
    }
    
    public function completedVoting($matchid){
        $maxmatchvotes = Mage::getStoreConfig('showdown/dagroup/maxmatchvotes');
    
        $rakevote = Mage::getModel('showdown/votes')->getCollection();
        $rakevote->addFieldToFilter('matchid', $matchid);
        $sticks = count($rakevote);
        if ($sticks >= $maxmatchvotes){ return true; } else { return false; }
    }
    
    public function checkWins($productid){
        $rake = Mage::getModel('showdown/votes')->getCollection();
        $rake->addFieldToFilter('votedfor', $productid);
        $rake->getSelect()->group('matchid');
        $sticks	 = count($rake);
        
        return $sticks;
    }
    
    public function dateExists($datenumber){
        $rake = Mage::getModel('showdown/matches')->getCollection();
        $rake->addFieldToFilter('datenumber', $datenumber);
        $sticks = count($rake);
        if ($sticks > 0){ return true; } else { return false; }
    }
    
    public function getProduct(){
        //$currCat = Mage::registry('current_category');
        $productId = 52;
        $product = Mage::getModel('catalog/product')->load($productId);
        return $product;
    }
    
    function getProduct2( Mage_Review_Model_Review $review ) {
        if( !isset($this->_loadedProducts[ $review->getEntityPkValue() ]) ) {
            $this->_loadedProducts[$review->getEntityPkValue()] = Mage::getModel('catalog/product')->load( $review->getEntityPkValue() );
        }
        
        return $this->_loadedProducts[ $review->getEntityPkValue() ];
    }
    
    public function getRatingAverages($product_id) {
        $rating = array();

        //$product_id = Mage::registry('product')->getId($productid);

        $resource = Mage::getSingleton('core/resource');
        $cn= $resource->getConnection('core_read');

        $sql = "select rt.rating_code, 
                avg(vote.percent) as percent from ".$resource->getTableName('rating_option_vote')." as vote 
                inner join ".$resource->getTableName('rating')." as rt
                on(vote.rating_id=rt.rating_id)
                inner join ".$resource->getTableName('review')." as rr
                on(vote.entity_pk_value=rr.entity_pk_value)
                where rt.entity_id=1 and vote.entity_pk_value=$product_id and rr.status_id=1
                group by rt.rating_code";
        $rating = $cn->fetchAll($sql);

        return $rating;
    }
    
    function getReviews() {
        $reviews = Mage::getModel('review/review')->getResourceCollection();
        $reviews->addStoreFilter( Mage::app()->getStore()->getId() )
                        ->addStatusFilter( Mage_Review_Model_Review::STATUS_APPROVED )
                        ->setDateOrder()
                        ->addRateVotes()
                        ->load();        
        
        return $reviews;
    }



    
}