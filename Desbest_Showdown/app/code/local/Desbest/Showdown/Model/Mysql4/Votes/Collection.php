<?php
class Desbest_Showdown_Model_Mysql4_Votes_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('showdown/votes');
    }
    
    public function addAttributeToSort($attribute, $dir=�asc�) 
    { 
        if (!is_string($attribute)) { 
        return $this; 
        } 
        $this->setOrder($attribute, $dir); 
        return $this; 
    }   
}