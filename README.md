# Showdown Magento Plugin

This magento plugin chooses 2 products at random and asks the user to vote between which one they like the best. The voting lasts for a month, the results are saved and the usee can see past contests and winners.

This plugin took 6 months to make.

## Credits

Copyright desbest 2012

## Features

* See past contents and which products won them
* Browse by date
* The two products chosen at random always belong to the same category.

## Compatibility

This was tested on Magento 1.7.0.2 (released in 2012). 

Magento 1.x is deprecated in favour of Magento 2, and that's not backwards compatible.